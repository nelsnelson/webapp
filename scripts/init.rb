#! /usr/bin/env ruby

$: << File.expand_path(File.dirname(__FILE__) + '/..')

require 'lib/extensions/migrations'
require 'lib/storage'

class Warriors < Database
end

Storage.new
Database::migrate Warriors.name

require 'app/models/account'
require 'app/models/tag'
require 'app/models/info'
require 'app/models/link'

AccountSetup.down
TaggedSetup.down
TagSetup.down
LinkSetup.down
InfoSetup.down

InfoSetup.up
LinkSetup.up
TagSetup.up
TaggedSetup.up
AccountSetup.up

