#! /usr/bin/env bash

remote=`git remote`
branch=`git rev-parse --abbrev-ref HEAD`
git add --all
git commit --amend --no-edit
git push -f ${remote} ${branch}

