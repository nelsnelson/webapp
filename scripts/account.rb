#! /usr/bin/env ruby

$: << File.expand_path(File.dirname(__FILE__) + '/..')

require 'sinatra/base'

Sinatra::Base::configure(:development) { }

def create_account(username, first_name, last_name, email, password)
  require 'lib/storage'
  require 'app/models/account'
  
  account = Account.find_or_create(:username => username) do |a|
    a.email = email 
    a.password = password 
    a.first_name = first_name 
    a.last_name = last_name 
  end
end

def list_accounts
  require 'lib/storage'
  require 'app/models/account'

  Account.each do |account|
    puts "  Account #{account.id}"
    puts "  Username: #{account.username}"
    puts "  First name: #{account.first_name}"
    puts "  Last name: #{account.last_name}"
    puts "  Email: #{account.email}"
    puts
  end
end

if __FILE__ == $0
  if ARGV.first == '--list'
    list_accounts
  else
    unless ARGV.length >= 5
      puts "Usage: account.rb <username> <first-name> <last-name> <email> <password>"
      puts "                  --list"
      exit
    end
    create_account(*ARGV)
  end
  exit
end

