#! /usr/bin/env bash

webhost='pasadena'
branch='master'

[[ -z "${*}" ]] && message='Quick update' || message="${*}"

git add --all
git commit -am "${message}"
git push origin master
ssh warriors@${webhost} "cd ~/public_html/public; git fetch origin; git reset --hard origin/${branch}"
