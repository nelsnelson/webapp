
require 'sinatra'
require 'sequel'
require 'json'
require 'erubis'
require 'maruku'

$KCODE = 'u' if RUBY_VERSION < '1.9'

# This defines where to look for the web application code.
set :root, File.dirname(__FILE__) + '/app'
# This defines where to find simple static web pages that do
# not require any runtime interpolation of application state
# information.
set :public_folder, File.dirname(__FILE__) + '/public'
# This defines which rendering engine to use for markdown files.
set :markdown, :layout_engine => :maruku, :layout => false

# When this web application is running in a development
# environment, use a randomly generated secret hash id
# to keep track of user sessions.
configure(:development) {
  set :bind, '0.0.0.0'
  require 'securerandom'
  set :session_secret, SecureRandom.hex
  enable :sessions
}

# If the runtime is JRuby, then set up some helpful things.
if defined? Java
class Java::JavaLang::Throwable
  def backtrace
    self.stack_trace.map(&:to_s).reject { |s| s =~ /(org\/jruby|org\.jruby|usr\/java|method_missing)/ }
  end
end

class Exception
  alias :ruby_backtrace :backtrace
  def backtrace
    self.ruby_backtrace.map(&:to_s).reject { |s| s =~ /(org\/jruby|org\.jruby|usr\/java|method_missing)/ }
  end
end
end # if defined? Java

# This defines a simple, basic Sinatra web application instance.
class Sinatra::Application
end

# Require (load) a modular Sinatra web application's code modules:
$: << '.' unless $:.include? '.'
[ "lib/**/*.{rb}",
  "#{settings.root}/helpers/**/*.{rb}",
  "#{settings.root}/routes/**/*.{rb}",
  "#{settings.root}/models/**/*.{rb}"
].each { |path|
  Dir.glob(path, &method(:require))
}

before do
  # Set the character set for HTML documents to unicode for
  # all content types for all requests.
  content_type :html, 'charset' => 'utf-8'
  # Remove the trailing slash ('/') of the URIs for all
  # requests.
  request.env['PATH_INFO'].gsub!(/\/$/, '')
end

# Whenever an end-user requests a resource with a URI which
# has not already been handled by a route defined above,
# treat the request as though it were a request for a
# resource which might exist, but which has not been
# explicitly defined.
#
# The reason for this is to support the fulfillment of static
# web pages as simply as possible.
#
# All a developer has to do is set up a particular resource
# as a directory with an index.erb file, like so:
#
#     webapp.com/resource/index.erb
#
get /\/([^\/]+)\/?/ do
  resource = "#{params[:captures].first}"
  erb :"/#{verify resource}/index"
end

# This defines the default home page.
get /\/?/ do
  erb :"default/index"
end

