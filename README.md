# Simple web application

Uses the small, sensible, ruby web application framework called Sinatra.

## Setup

This particular project uses PostgreSQL as a database backend.

To use Postgres, you will need to install both the PostgreSQL database server, and adapter software so that the Ruby web application can interface with the server.


### On Ubuntu 14.04

    sudo apt-get install --fix-missing --assume-yes make ruby1.9.1-dev postgresql libpq-dev

The PostgreSQL server configuration file is often found someplace like `/etc/postgresql/9.3/main/pg_hba.conf`.


### On Mac OS X El Capitan

Download Postgres.app from http://postgresapp.com/.  This is hands down the easiest way to get started with PostgreSQL on the Mac.

Install the `pg` adapter:

    env ARCHFLAGS="-arch x86_64" gem install pg

The PostgreSQL server configuration file is often found someplace like `$HOME/Library/Application\ Support/Postgres/var-9.5`.


### All operating systems

Don't forget to modify your `pg_hba.conf` file to ensure that it trusts your local connections.

An example of a trusting `pg_hba.conf`: https://bitbucket.org/snippets/nelsnelson/b4zM8


## Install the web application 

Much like the way that Python projects may define dependencies in a `requirements.txt` file, a Ruby project may define dependencies in a `Gemfile`.

To manage the installation of those dependencies from a Gemfile, use `bundler`.

    sudo gem install bundler

Run bundler to install the third-party library gems required by the project.

    bundler

Finally, initialize the database with some simplistic migrations.

    scripts/init.rb

You should now be able to run the web application.

    ruby app.rb

