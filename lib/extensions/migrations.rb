
require 'lib/storage'

module Sequel
  class Migration
    def initialize(db)
      @db = db
    end
    def self.apply(db, direction)
      raise(ArgumentError, "Invalid migration direction specified (#{direction.inspect})") unless [:up, :down].include?(direction)
      new(db).send(direction)
    end
    def self.up
      apply(Sequel::Model.db, :up)
    end
    def self.down
      apply(Sequel::Model.db, :down)
    end
    
    # Intercepts method calls intended for the database and sends them along.
    def method_missing(method_sym, *args, &block)
      @db.send(method_sym, *args, &block)
    end

    # This object responds to all methods the database responds to.
    def respond_to_missing?(meth, include_private)
      @db.respond_to?(meth, include_private)
    end
  end
end

module Sequel
  module DatabaseHelpers
    def class_derived_database_name
      self.class.name.split(/::/).last.sub(/Database/, '')
    end

    def implicit_database_name
      underscore(demodulize(class_derived_database_name)).to_sym rescue self.class.name
    end

    def database
      implicit_database_name
    end
  end
end

module Sequel
  module Plugins
    module IdentityMap
      module ClassMethods
        # Returns the global thread-safe identity map.
        def identity_map
          puts "Getting identity map"
          GlobalSequelIdentityMap
        end

        private

        # Set the thread local identity map to the given value.
        def identity_map=(v)
          silence_warnings do
            puts "Setting identity map"
            Object.instance_eval { const_set :GlobalSequelIdentityMap, v }
          end
        end
      end
    end
  end
end if defined? GlobalSequelIdentityMap

module Sequel
  module Postgres
    class Initialization < Sequel::Migration
      include Sequel::Inflections
      include SQLHelpers
      include DatabaseHelpers

      def up
        create_user
        create_database
        switch_database
        switch_user
      end

      def down
        switch_database :postgres
        drop_database
        delete_user
      end

      def database_exists?(database_name=database)
        results = execute "select count(*) > 0 from pg_catalog.pg_database where datname = '#{database_name}';"
        results.first.values.first == true
      end

      def user_exists?(username=database)
        results = execute "select count(*) > 0 from pg_shadow where usename = '#{username}';"
        results.first.values.first == true
      end

      def connected_database
        return nil unless Sequel::Model.db
        Sequel::Model.db.opts[:uri].lines.grep(/\:\/\/[\w]+(\:[\d]+|\/)([\w]+)/) { $2 }.first
      end

      def connected_username
        return nil unless Sequel::Model.db
        Sequel::Model.db.opts[:uri].lines.grep(/user=([\w]+)/) { $1 }.first
      end

      def switch_database(database_name=database)
        uri = Sequel::Model.db.opts[:uri] || ''
        schema   = uri.lines.grep(/^([\w]+(\:[\w]+)?)\:\/\//) { $1 }.first || 'jdbc:postgresql'
        host     = uri.lines.grep(/\:\/\/([\w]+)/) { $1 }.first || localhost
        port     = uri.lines.grep(/\:\/\/[\w]+(\:?([\d]+))/) { $1 }.first
        username = uri.lines.grep(/user=([\w]+)/) { $1 }.first || database_name

        Sequel::Model.db.disconnect if Sequel::Model.db
        Sequel::Model.db = Sequel.connect("#{schema}://#{host}#{port ? ":#{port}" : ""}/#{database_name}?user=#{username}")
        Sequel::Model.descendants.each { |model| model.db = Sequel::Model.db }
        return true
      end

      def switch_user(username=database, database_name=nil)
        uri = Sequel::Model.db.opts[:uri] || ''
        schema        = uri.lines.grep(/^([\w]+(\:[\w]+)?)\:\/\//) { $1 }.first || 'jdbc:postgresql'
        host          = uri.lines.grep(/\:\/\/([\w]+)/) { $1 }.first || localhost
        port          = uri.lines.grep(/\:\/\/[\w]+(\:?([\d]+))/) { $1 }.first
        database_name = uri.lines.grep(/\/([\w]+)\??([\w]+\=[\w]+)*$/) { $1 }.first || database

        Sequel::Model.db.disconnect if Sequel::Model.db
        Sequel::Model.db = Sequel.connect("#{schema}://#{host}#{port ? ":#{port}" : ""}/#{database_name}?user=#{username}")
        Sequel::Model.descendants.each { |model| model.db = Sequel::Model.db }
        return true
      end

      def create_database(database_name=database, username=database)
        begin
          puts "Creating database: #{database_name}"
          create_user username unless user_exists? username
          run "create database #{database_name} owner #{username}"
        rescue Exception => error
          if error.to_s =~ /ERROR: database "#{database_name}" already exists/
            unless defined? tried_once_already
              drop_database database_name
              tried_once_already = true
              retry
            end
          end
          puts "#{error.inspect}"
          return false
        end unless database_exists? database_name
      end

      def drop_database(database_name=database)
        begin
          puts "Dropping database: #{database_name}"
          run "drop database #{database_name}"
        rescue Sequel::DatabaseError => error
          puts "#{error.inspect}"
          return false
        rescue Exception => error
          if error.to_s =~ /ERROR: cannot drop the currently open database/
            results = execute "select procpid from pg_stat_activity where datname = '#{database_name}'"
            for row in results
              puts "You must first kill process with pid #{row['procpid']}"
              #`sudo kill -9 #{row['procpid']}`
            end
            puts "#{error.inspect}"
          end
          return false
        end if database_exists? database_name
      end

      def create_user(username=database)
        # 
        begin
          puts "Creating user: #{username}"
          run "create user #{username}"
          run "alter role #{username} with password '#{username}'"
          return true
        rescue Sequel::DatabaseError => error
          puts "#{error.inspect}"
          return false
        rescue Exception => error
          puts "#{error.inspect}"
          return false
        end unless user_exists? username
      end

      def delete_user(username=database)
        begin
          puts "Dropping user: #{username}"
          run "reassign owned by #{username} to postgres"
          run "drop user #{username}"
        rescue Sequel::DatabaseError => error
          puts "#{error.inspect}"
          return false
        rescue Exception => error
          puts "#{error.inspect}"
          return false
        end if user_exists? username
      end

      def self.migrate(schema_name)
        [ :down, :up ].each { |m|
          Kernel.const_get(schema_name).send m
        }
      end
    end
  end
end

class Database < Sequel::Postgres::Initialization
end
