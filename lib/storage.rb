
begin
require 'java'
rescue LoadError => ignore
end if RUBY_PLATFORM =~ /java/i
require 'rubygems'
require 'sequel'
require 'logger'
require 'yaml'
require 'etc'

if defined? CONCURRENT_CACHING_ENABLED
import java.util.concurrent.ConcurrentHashMap

class ConcurrentHashMap
  def set(key, value, ttl)
    self.put(key, value)
  end
end

GlobalSequelIdentityMap = ConcurrentHashMap.new
GlobalMemcachedMap = ConcurrentHashMap.new

Sequel::Model.plugin :identity_map
Sequel::Model.plugin :caching, GlobalMemcachedMap, :ttl=>1800
Sequel.extension :migration
end # if defined? CONCURRENT_CACHING_ENABLED

class Storage
  def initialize
    gem 'jdbc-postgres' if RUBY_PLATFORM =~ /java/i

    if defined? Sinatra::Base.settings
      environment = Sinatra::Base.settings.environment
      root = File.expand_path("#{Sinatra::Base.settings.root}")
    else
      environment = 'default'
      root = File.expand_path("#{File.dirname(__FILE__)}/..")
    end

    configuration_file = File.expand_path("#{root}/config/database.yml")
    conf = YAML::load(File.open(configuration_file))[environment.to_s]
    url = "#{conf['adapter']}://#{conf['host']}/#{conf['database']}?user=#{conf['username']}"
    url += "&password=#{conf['password']}" if conf.include? 'password'
    url.insert(0, 'jdbc:') if Gem.loaded_specs.include? 'jdbc-postgres'

    puts "Connecting to database: #{url}"
    Sequel::Model.db = Sequel.connect(url)
  end
end

module Sequel
  class Model
    # Returns the list of Model descendants.
    def self.descendants
      @descendants ||= []
    end

    # Adds the new model class to the list of Model descendants.
    def self.inherited(subclass)
      descendants << subclass
      super
    end

    module ClassMethods
      def implicit_table_name
        underscore(demodulize(name)).to_sym
      end
    end
  end
end

module Sequel
  module SQLHelpers
    def execute(sql)
      Sequel::Model.db[sql]
    end

    def run(sql)
      Sequel::Model.db.run(sql)
    end
  end
end

Storage.new

