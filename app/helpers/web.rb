
require 'open-uri'

module App
module Web
  def webpage(url)
    webpage = ""
    begin
      logger.info "Connecting to #{url}"
      webpage = open(URI.escape(url), 'User-Agent' => 'ruby-wget').read
    rescue RuntimeError => e
      logger.error "The request for a page at #{url} failed because of a problem with the OpenURI gem."
      if (e.message =~ /redirection forbidden/)
        logger.error "You are trying to access a resource that requires redirection to another site."
        logger.error e.backtrace
      end
    rescue Errno::ECONNREFUSED => e
      logger.error "The request for a page at #{url} encountered a refused connection"
    rescue SocketError => e
      logger.error "The request for a page at #{url} could not be completed"
    rescue Timeout::Error => e
      logger.error "The request for a page at #{url} timed out"
    rescue OpenURI::HTTPError => e
      logger.error "The request for a page at #{url} returned an error: #{e.message}"
    rescue Exception => e
      logger.error "The request for a page at #{url} encountered an unexpected error: #{e.message}"
    end
    webpage
  end
end
end

helpers App::Web

