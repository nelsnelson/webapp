
module Sinatra::Cookies
  class Jar
    def initialize(request, response)
      super()
      @request = request
      @response = response
    end

    def []=(key, value)
      @response.set_cookie(
        key.to_s,
        :value => value,
        :host => "#{@request.host}",
        :path => '/'
      )
    end

    def [](key)
      @request.cookies[key.to_s]
    end
  end

  def cookies
    @cookies ||= Jar.new request, response
  end
end

helpers Sinatra::Cookies
