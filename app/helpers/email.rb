
module Sinatra::Email
  def send_email(to, from, subject, body_path, params)
    begin
      mail = Pony.mail(
        :via  => :smtp,
        :via_options => {
          :address              => 'smtp.gmail.com',
          :port                 => '587',
          :enable_starttls_auto => true,
          :user_name            => 'warriors-activation@nelsnelson.org',
          :password             => '3fo49&d$j#x&l-p',
          :authentication       => :plain,
          :domain               => params[:domain]
        },
        :to        => to,
        :from      => from,
        :subject   => subject,
        :html_body => erb(:"#{body_path}.html", :locals => params),
        :body      => erb(:"#{body_path}.txt", :locals => params)
      )
    rescue Exception => ex
      puts "Error posting email: #{ex}"
      puts ex.backtrace
      return ex.message
    end if defined? Pony
    nil
  end
end

helpers Sinatra::Email
