# Link

require 'app/models/info'

class LinkSetup < Sequel::Migration
  def up
    create_table! :link do
      primary_key :id
      foreign_key :from_id, :info, :on_delete => :cascade
      foreign_key :to_id,   :info, :on_delete => :cascade

      String :name
    end unless table_exists? :link
  end

  def down
    drop_table :link if table_exists? :link
  end
end if defined? Sequel::Migration

# LinkSetup.down
# LinkSetup.up

class Link < Sequel::Model
  many_to_one :from, :class => Info, :key => :from_id
  many_to_one :to,   :class => Info, :key => :to_id

  def to_s
    "#{name} -> #{to.name} [#{to.identity}]"
  end
end

module Linkable
  def link(link_name, obj=nil)
    link = Link.first(:name => link_name.to_s, :from_id => self.id)
    link.to.refresh if link
    return link unless obj
    if link
      link.to = obj
      link.save
    else
      link = Link.find_or_create(:name => link_name.to_s, :from_id => self.id, :to_id => obj.id) { |l|
        l.from = self
        l.to = obj
      }
    end
    link
  end

  def linked?(link_name)
    Link.filter(:name => link_name.to_s, :from_id => self.id).count > 0
  end

  def unlink(link_name)
    return unless linked? link_name
    link = Link.first(:name => link_name.to_s, :from_id => self.id)
    to = link.to
    link.delete
    to
  end

  def linkto(link_name)
    link(link_name).to if linked? link_name
  end

  def linksfrom(link_name=nil)
    if link_name
      Link.filter(:name => link_name.to_s, :to_id => self.id).map { |link| link.from }
    else
      Link.filter(:to_id => self.id).map { |link| link.from }
    end
  end

  def respond_to?(method)
    m = method.to_s
    mname = m =~ /\=$/ ? m.chop : m

    return super unless m =~ /(with_key|_to)$/

    linked? mname
  end

  def method_missing(method, *args, &block)
    m = method.to_s
    mname = m =~ /\=$/ ? m.chop : m

    if args.empty?
      linkto(mname) || super
    elsif (mname =~ /_to$/ or m =~ /\=$/) and args.first.inform?
      link mname, args.first
      args.first
    else
      super
    end
  end
end # module Linkable
