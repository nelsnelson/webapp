
Entry = Struct.new :datum, :expire_time, :last_updated

class Cache < Hash
  def self.cache
    @@cache ||= Cache.new
  end

  def self.expired?(entry)
    return true unless entry
    Time.new.to_i - entry.last_updated > entry.expire_time
  end

  def self.get(key, value=nil, expire_time=500, &block)
    entry = cache[key]
    entry = self.put(key, value, expire_time, &block) if expired? entry
    entry ? entry.datum : nil
  end

  def self.put(key, value=nil, expire_time=500, &block)
    value = yield(key) if block and not value
    entry = cache[key] = Entry.new value, expire_time, Time.new.to_i
    entry ? entry.datum : nil
  end

  def self.[](key)
    self.get key
  end

  def self.[]=(key, value)
    self.put key, value, 500
  end

  def self.clear(key)
    entry = self.get key
    self.put key, nil, -1
    entry ? entry.datum : nil
  end
end
