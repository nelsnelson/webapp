# Account

class AccountSetup < Sequel::Migration
  def up 
    create_table! :account do
      primary_key :id

      String  :email
      String  :username
      String  :password
      String  :first_name
      String  :last_name
      Boolean :accepted_eula
    end unless table_exists? :account
  end

  def down
    drop_table :account if table_exists? :account
  end
end if defined? Sequel::Migration

# AccountSetup.down
# AccountSetup.up

class Account < Sequel::Model
  many_to_many :information, :join_table => :info, :key => :info_id

  def full_name
    first_name + " " + last_name
  end

  def << (info)
    add_information info
  end
end
