# Tag

class TagSetup < Sequel::Migration
  def up 
    create_table! :tag do
      primary_key :id

      String  :name, :unique => true, :null => false
      index   :name
    end unless table_exists? :tag
  end

  def down
    drop_table(:tag, :cascade => true) if table_exists? :tag
  end
end if defined? Sequel::Migration

# TagSetup.down
# TagSetup.up

class Tag < Sequel::Model
  def to_s
    name
  end

  def self.tidy
    db << %Q{delete from tag where id in 
(select a.id from tag a group by a.id, a.name 
having ((select count(b.id) from tagged b 
where b.tag_id = a.id) = 0))}
    return nil
  end

  def self.dirty
    results = db.fetch %Q{select a.id, a.name from tag a group by a.id, a.name having 
((select count(b.id) from tagged b where b.tag_id = a.id) = 0)}
    return nil if results.empty?
    s = [ "%5s %20s" % results.first.keys ]
    s.concat results.collect{ |row| "%5d %20s" % row.values }
    s.join("\n")
  end

  def self.stats
    results = db.fetch %Q{select a.*, count(a.id) as "number tagged" 
from tag a, tagged b where b.tag_id = a.id group by a.id, a.name}
    return nil if results.empty?
    s = [ "%5s %20s %15s" % results.first.keys ]
    s.concat results.collect{ |row| "%5d %20s %15s" % row.values }
    s.join("\n")
  end
end
class Tagged < Sequel::Model
end

# Tagged

class TaggedSetup < Sequel::Migration
  def up
    create_table! :tagged do
      primary_key :id
      foreign_key :info_id, :info, :on_delete => :cascade
      foreign_key :tag_id, :tag, :on_delete => :cascade
    end unless table_exists? :tagged
  end

  def down
    drop_table(:tagged, :cascade => true) if table_exists? :tagged
  end
end

# TaggedSetup.down
# TaggedSetup.up

# Taggable

module TagHelpers
  def attributes;     tags;                    end
  def has(*args);     tag *args;               end
  def hasnt(*args);   untag *args;             end
  def has?(*args);    tagged_with_all?(*args); end
  def hasany?(*args); tagged_with_any? *args;  end
  def hasnt?(*args);  tagged_with_none? *args; end
  alias :has!  :hasnt
end

module Taggable
  include TagHelpers
  def tag(*args)
    args.flatten!
    args.collect!(&:to_s)
    for tag in args - (nil_safe_tags & args)
      add_tag Tag.find_or_create(:name => tag)
    end
    save
    self
  end

  def untag(*args)
    args.flatten!
    args.collect!(&:to_s)
    for tag in nil_safe_tags & args
      remove_tag Tag.find(:name => tag)
    end
    save
    self
  end
  alias :tag! :untag

  def tagged_with?(tag)
    tagged_with_any? tag
  end

  def tagged_with_all?(*args)
    args.flatten!
    args.collect!(&:to_s)
    (nil_safe_tags & args).length == args.length
  end

  def tagged_with_any?(*args)
    args.flatten!
    args.collect!(&:to_s)
    not self.tagged_with_none? *args
  end

  def tagged_with_none?(*args)
    args.flatten!
    args.collect!(&:to_s)
    (nil_safe_tags & args).empty?
  end

  def nil_safe_tags
    tags ? tags.map(&:to_s) : []
  end
end # module Taggable
