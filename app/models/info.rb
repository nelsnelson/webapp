

class InfoSetup < Sequel::Migration
  def up 
    create_table? :info do
      primary_key :id
      foreign_key :parent_id, :info, :on_delete => :set_null
      index :name

      String :name
      String :short_name
      String :display_name
      String :description
      String :info_type, :null => false
      String :properties, :default => {}.to_yaml.strip
    end
  end

  def down
    drop_table(:info, :cascade => true) if table_exists? :info
  end
end if defined? Sequel::Migration

# InfoSetup.down
# InfoSetup.up

def Info(name, &block)
  Info.find(:name => name) || Info.new(:name => name, &block)
end

class Info < Sequel::Model
  plugin :rcte_tree
  plugin :serialization
  plugin :single_table_inheritance, :info_type
  #plugin :tactical_eager_loading
  one_to_many :links, :join_table => :link, :key => :from_id
  one_to_many :tagged, :key => :info_id
  many_to_many :tags, :join_table => :tagged, :right_key => :tag_id, :right_primary_key => :id
  serialize_attributes :yaml, :properties

  def initialize(*args, &block)
    super(*args)
    save
    self.with &block if block
  end
end
