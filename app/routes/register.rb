
post /register\/?/ do
begin
  result = {}
  params = request.POST
  first    = params['first']
  last     = params['last']
  email,to = params['email']
  username = params['username']
  password = params['password']
  confirm  = params['confirm']
  salutation = (first and last) ? ", #{first} #{last}" : ''
  to = email
  from = 'warriors-activation@nelsnelson.org'
  subject = "Thanks for signing up#{salutation}!"
  body = 'email/activation'
  account = Account.find(:email => email) || Account.find(:username => username)

  if account
    status 409
    result[:message] = 'That user is already registered'
  elsif email.empty? or not email =~ /@/
    status 400
    result[:message] = 'Please enter an email address'
  elsif username.empty?
    status 400
    result[:message] = 'Please enter a username'
  elsif password.empty?
    status 400
    result[:message] = 'Please enter a password'
  elsif confirm.empty?
    status 400
    result[:message] = 'Please confirm the password'
  elsif password != confirm
    status 400
    result[:message] = 'The passwords must match'
  else
    account = {
      :email => email,
      :username => username,
      :password => password
    }

    require 'securerandom' unless defined? SecureRandom
    token = SecureRandom.uuid
    Cache.put token, account, 5000
    message = send_email to, from, subject, body, {
      :token => token, :domain => request.host, :port => request.port
    }

    session[:activation_pending] = true
    cookies[:activation_pending] = session[:activation_pending]
    result[:username] = account[:username]
    result[:message] = message || 'Thanks for signing up!'
  end

  result.to_json
rescue Exception => ex
  logger.error ex
  { :error => ex.message }.to_json
end
end

get /activation\/?/ do
begin
  params = request.GET
  token = params['token']
  if (props = Cache[token]) and (account = Account.create(props))
    session[:username] = account[:username]
    cookies[:username] = session[:username]
    redirect to(:"profile/#{account.username}")
  else
    halt 403, 'Sorry, that activation key is not valid'
  end
rescue Exception => ex
  logger.error ex
  { :error => ex.message }.to_json
ensure
  Cache[token] = nil
  session[:activation_pending] = false
  cookies[:activation_pending] = session[:activation_pending]
end
end
