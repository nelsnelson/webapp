
post /login\/?/ do
begin
  result = {}
  params = request.POST
  ip = request.ip
  account =
    Account.find(:username => params['userid'], :password => params['password']) ||
    Account.find(:email => params['userid'], :password => params['password'])

  if account
    Cache[:failed_login_attempt] ||= {}
    Cache[:failed_login_attempt][ip] ||= nil
    session[:username] = account.username
    cookies[:username] = session[:username]
    result[:username] = account.username
    result[:message] = 'Login successful!'
  else
    Cache[:failed_login_attempt] ||= {}
    Cache[:failed_login_attempt][ip] ||= { :time => Time.now, :n => 0 }
    if Cache[:failed_login_attempt][ip][:n] > 3
      puts Cache[:failed_login_attempt].inspect
      status 403
      result[:message] = 'Too many failed login attempts'
    else
      Cache[:failed_login_attempt][ip][:n] += 1
      Cache[:failed_login_attempt][ip][:time] = Time.now
      status 401
      result[:message] = 'Invalid credentials'
    end
    sleep [ Random.rand*3.5, 1.5 ].max
  end

  result.to_json
rescue Exception => ex
  logger.error ex
  { :error => ex.message }.to_json
end
end

get /logout\/?/ do
  session.clear
  cookies[:username] = nil
  redirect to('/')
end

get /forgot\/?/ do
end
