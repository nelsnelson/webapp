
get '/profile/:username' do
begin
  puts "session[:username] #=> #{session[:username]}"
  if params[:username] != session[:username]
    redirect to('/')
    return
  end
  if (account = Account.find(:username => params[:username]))
    erb :'profile/index', :locals => { :account => account }
  else
    status 404
  end
rescue Exception => ex
  logger.error ex
  { :error => ex.message }.to_json
end
end

post '/profile/?:username?' do
begin
  if params[:username] != session[:username]
    redirect to('/')
    return
  end
  unless (account = Account.find(:username => params[:username]))
    status 404
    return
  end
  params.merge! request.POST
  account.first_name = params[:first]
  account.last_name = params[:last]
  account.email = params[:email]
  account.username = params[:username]
  account.save
  session[:username] = account.username
  cookies[:username] = session[:username]
  redirect "/profile/#{account.username}"
rescue Exception => ex
  logger.error ex
  { :error => ex.message }.to_json
end
end

post '/password/?:username?' do
begin
  if params[:username] != session[:username]
    redirect to('/')
    return
  end
  account = Account.find(:username => params['username'], :password => params['password'])
  unless account
    status 403
    return
  end
  params.merge! request.POST
  if params[:new] == params[:confirm]
    account.password = params[:new]
    account.save
  end
  redirect "/profile/#{account.username}"
rescue Exception => ex
  logger.error ex
  { :error => ex.message }.to_json
end
end
