var app = angular.module('app', []);

app.controller('appController', function($scope){
    $scope.message = "This is the message from the parent controller scope!";
    $scope.fromModal = {};
})

app.directive('modalDirective', function($http, $compile){
    return {
        restrict:"A",
        scope:{
            template:"@",
            title:"@",
            modalId:"@",
            input:"=",
            output:"="
        },
        link:function(scope, element, attrs){


            element.click(function(){
                var template;
                var modal = scope.template;

                $http.get(modal).then(function(tmpl) {
                    console.log(tmpl)
                    template = $compile(tmpl.data)(scope);
                    template.attr('id', scope.modalId)
                    element.after(template);

                    var theModal = $('#'+template.attr('id')).modal('show');

                    theModal.on('hidden.bs.modal', function(){
                        theModal.remove();
                    });

                });
            })

        }
    }
})