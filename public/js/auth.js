
$(document).ready(init_auth);

function init_auth() {
    $('a#auth').click(auth);
    $('a#deauth').click(deauth);
    $('a.close').click(close);
    $('form#login input[type=submit]#login-submit').click(login);
    $('form#signup input[type=submit]#signup-submit').click(signup);
    $('form#login').submit(login);
    $('form#signup').submit(signup);
    $('form input').focus(toggleLabel);
}

var auth = function() {
    var signupOrLogin = $('div#signup-or-login');
    $('div.dialog').append(signupOrLogin);
    $(signupOrLogin).fadeIn();
    $('div#signup-or-login input:first').select().focus();
    return false;
}

var deauth = function() {
    logout();
    return false;
}

var close = function() {
    var signupOrLogin = $('div#signup-or-login');
    var nav = $('div.nav');
    var fields = $(':input[type="text"],:input[type="email"],:input[type="password"]');
    signupOrLogin.fadeOut(80);
    nav.after(signupOrLogin);
    fields.each(function() { $(this).val(''); });
    clearMessages();
    return false;
}

var signup = function(e) {
    var target = e.originalEvent || e.originalTarget;
    var source = $(target.srcElement || target.originalTarget).attr('id');
    if (source == 'signup-email' || 
        source == 'signup-username' || 
        source == 'signup-password' || 
        source == 'signup-confirm' || 
        source == 'signup-submit')
    {
        e.preventDefault();
    }
    clearMessages();
    try {
        $.ajax({
            type: 'POST',
            url: '/register',
            data: $('form#signup').serialize(),
            success: signupSuccess,
            failure: signupFailure,
            error: signupError
        });
    } catch (ex) {
        $('div#signup').find('.result').html(ex);
        console.log('Error: ' + ex);
    }
    return false;
}

var login = function(e) {
    var target = e.originalEvent || e.originalTarget;
    var source = $(target.srcElement || target.originalTarget).attr('id');
    if (source == 'login-username' || 
        source == 'login-password' || 
        source == 'login-submit')
    {
        e.preventDefault();
    }
    clearMessages();
    try {
        abortIfBlank('form#login');
        $.ajax({
            type: 'POST',
            url: '/login',
            data: $('form#login').serialize(),
            success: loginSuccess,
            failure: loginFailure,
            error: loginError
        });
    } catch (ex) {
        $('div#login').find('.result').html(ex);
        console.log('Error: ' + ex);
    }
    return false;
}

var logout = function() {
    try {
        $.ajax({
            type: 'GET',
            url: '/logout',
            success: logoutSuccess,
            failure: logoutFailure,
            error: logoutError
        });
    } catch (ex) {
        console.log("Error: " + ex);
    }
    return false;
}

var signupSuccess = function(response, code, xhr) {
    var data = $.parseJSON(response);
    console.log(data);
    if (data['username'] != null) {
        cookie('username', data['username'], 14440);
        $('div#login input[type=text],div#login input[type=email],div#login input[type=password]').val('');
        $('div#signup').find(':button').select().focus();
        $('div#signup').find('.result').html(data['message']);
        $('a#auth').hide();
        $('a#deauth').show();
        $('a.close').delay(1250, 'a.close').queue('a.close', function() { 
            $(this).trigger('click');
        }).dequeue('a.close');
    } else {
        $('div#login').find(':input#password').each(function() { $(this).val(''); });
        $('div#login').find(':input#confirm').each(function() { $(this).val(''); });
        $('div#login').find(':input#password').select().focus();
        $('div#signup').find('.result').html(data['message']);
    }
}

var signupFailure = function(response, code, xhr) {
    $('div#signup').find('.result').html(data['message']);
}

var signupError = function(xhr, code) {
    var data = $.parseJSON(xhr.responseText);
    console.log(data);
    if (xhr.status == 400 || xhr.status == 409) {
        $('div#signup').find('.result').html(data['message']);
    }
}

var loginSuccess = function(response, code, xhr) {
    var data = $.parseJSON(response);
    console.log(data);
    if (data['username'] != null) {
        cookie('username', data['username'], 14440);
        $('div#login input[type=text],div#login input[type=password]').val('');
        $('div#login').find(':button').select().focus();
        $('div#login').find('.result').html(data['message']);
        $('a#auth').hide();
        $('a#deauth').show();
        $('a.close').delay(1250, 'a.close').queue('a.close', function() { 
            $(this).trigger('click');
        }).dequeue('a.close');
    } else {
        $('div#login').find(':input#password').each(function() { $(this).val(''); });
        $('div#login').find(':input#password').select().focus();
        $('div#login').find('.result').html(data['message']);
    }
}

var loginFailure = function(response, code, xhr) {
    $('div#signup').find('.result').html(data['message']);
}

var loginError = function(xhr, code) {
    var data = $.parseJSON(xhr.responseText);
    console.log(data);
    if (xhr.status == 401 || xhr.status == 403) {
        $('div#login').find('.result').html(data['message']);
    }
}

var logoutSuccess = function() {
    uncookie('username');
    $('a#auth').show();
    $('a#deauth').hide();
    document.location = '/';
}

var logoutFailure = function() {
    uncookie('username');
}

var logoutError = function() {
    uncookie('username');
}

var findlabel = function(input) {
    var label = $("label[for='" + $(input).attr('id') + "']");
    if (label.length == 0) {
        return input.closest('label');
    }
    return label;
}

var validate = function(form) {
    $(form +' input').each(function() {
        var field = $(this);
        var label = findlabel(field);
        if (label && $.trim(field.val()) == '') {
            throw 'Please enter a ' + $.trim(label.text()).toLowerCase();
        }
    });
}

var abortIfBlank = function(form) {
    $(form +' input').each(function() {
        var field = $(this);
        var label = findlabel(field);
        if (label && $.trim(field.val()) == '') {
            throw 'Please enter a ' + $.trim(label.text()).toLowerCase();
        }
    });
}

var toggleLabel = function() {
    
}

var clearMessages = function() {
    $('div#signup div.result, div#login div.result').html('');
}

function focusFirstInputChild() {
    var el = $(this);
    var input = el.find('input:first');
    selectEnd(input);
}

function selectEnd(field) {
    var l = field.val().length;
    if (l > 0) createSelection(field, l, l);
    else {
        field.select(); 
        field.focus();
    }
}

function cookie(name, value, expires) {
    var expires = '';
    if (expires) {
        var date = new Date();
        date.setTime(date.getTime() + expires);
        expires = '; expires=' + date.toGMTString();
    }
    if (value) {
        document.cookie = name + '=' + value + expires + '; path=/';
    } else {
        var nameEQ = name + '=';
        var ca = document.cookie.split(';');
        for (var i=0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1,c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length,c.length);
            }
        }
        return null;
    }
}

function uncookie(name) {
    cookie(name, '', -1);
}
